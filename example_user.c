#include <math.h>
#define MIBS_IMPL
#include "mibs.h"
#include "raylib.h"
#include "kan.h"
#include "state.h"

State* state = 0;

#define RECT_WIDTH  100
#define RECT_HEIGHT 100

#define MOVE_RECT_TASK1_ID 0
typedef Kan_Task((struct {
    float x,y;
    Color color;
    float direction;
})) Move_Rect_Task1;

Move_Rect_Task1 *move_rect_task1 = 0;

void move_rect_task1_update(void* data)
{
    Move_Rect_Task1* task = data;

    if (!task->base.complete) {
        float t = GetTime();
        int w = GetScreenWidth();
        int h = GetScreenHeight();
        int rw = RECT_WIDTH/2;
        int rh = RECT_HEIGHT/2;
        int x = ((w/4 * 3 - rw) - (w/4 + rw));
        int y = ((h/4 * 3 - rh) - (h/4 + rh));
        task->state.x = w/4 - rw + x*(sinf(t*2)+1)*0.5f;
        task->state.y = h/4 - rh + y*(sinf(t*2)+1)*0.5f;
    }
    DrawRectangle(task->state.x, task->state.y, RECT_WIDTH, RECT_HEIGHT, task->state.color);
}

#define MOVE_RECT_TASK2_ID 1
typedef Kan_Task((struct {
    float x,y;
    Color color;
    float direction;
})) Move_Rect_Task2;

Move_Rect_Task2 *move_rect_task2 = 0;

void move_rect_task2_update(void* data)
{
    Move_Rect_Task2* task = data;

    if (!task->base.complete) {
        float t = GetTime();
        int w = GetScreenWidth();
        int h = GetScreenHeight();
        int rw = RECT_WIDTH/2;
        int rh = RECT_HEIGHT/2;
        int x = ((w/4 * 3 - rw) - (w/4 + rw));
        int y = ((h/4 * 3 - rh) - (h/4 + rh));
        task->state.x = w/4 * 3 - rw - x*(sinf(t*2)+1)*0.5f;
        task->state.y = h/4 * 3 - rh - y*(sinf(t*2)+1)*0.5f;

    }
    DrawRectangle(task->state.x, task->state.y, RECT_WIDTH, RECT_HEIGHT, task->state.color);
}

bool handle_keyboard_events(void)
{
    bool reload_requested = IsKeyPressed(KEY_R);

    if (IsKeyPressed(KEY_SPACE)) {
        state->running = !state->running;
    }

    return reload_requested;
}

void progress_tasks(void)
{
    for (size_t i = 0; i < state->tasks.count; i++) {
        Kan_Task_Base* b = &state->tasks.items[i];
        if (!b->complete) b->update(b->self_ptr);
    }
}

void draw(void)
{
    BeginDrawing();
    
    ClearBackground(state->bg_color);

    progress_tasks();

    EndDrawing();
}

void user_set_state(State *state_) { state = state_; }

void user_init(void)
{
    state->bg_color = RED;

    move_rect_task1 = kan_new_task(Move_Rect_Task1, state, MOVE_RECT_TASK1_ID, move_rect_task1_update);
    move_rect_task1->state.x = GetScreenWidth()/4 - RECT_WIDTH/2;
    move_rect_task1->state.y = GetScreenHeight()/4 - RECT_HEIGHT/2;
    move_rect_task1->state.color = GREEN;
    move_rect_task1->state.direction = 1.0f;

    move_rect_task2 = kan_new_task(Move_Rect_Task2, state, MOVE_RECT_TASK2_ID, move_rect_task2_update);
    move_rect_task2->state.x = GetScreenWidth()/4 * 3 - RECT_WIDTH/2;
    move_rect_task2->state.y = GetScreenHeight()/4 * 3 - RECT_HEIGHT/2;
    move_rect_task2->state.color = BLUE;
    move_rect_task2->state.direction = -1.0f;
}

void user_deinit(void)
{
    kan_da_deinit(&state->tasks);
    kan_da_deinit(&state->alloced_tasks);
}

void user_after_reload(void)
{
    for (size_t i = 0; i < state->tasks.count; i++) {
        Kan_Task_Base *b = &state->tasks.items[i];
        switch (b->id) {
            case MOVE_RECT_TASK1_ID: {
                b->update = &move_rect_task1_update;
                b->self_ptr = state->alloced_tasks.items[i];
            } break;
            case MOVE_RECT_TASK2_ID: {
                b->update = &move_rect_task2_update;
                b->self_ptr = state->alloced_tasks.items[i];
            } break;
        }
    }
}

bool user_new_frame(void)
{
    bool reload_requested = handle_keyboard_events();
    draw();

    return reload_requested;
}

