#define MIBS_IMPL
#include "mibs.h"
#define KAN_IMPL
#include "kan.h"
#include "raylib.h"
#include "state.h"

#define USER_SO "build/libexample_user.so"

int main(void)
{
    InitWindow(1000, 1000, "Kamkow ANimations");
    mdefer { CloseWindow(); }
    
    // initial state
    State state = {0};
    state.running = true;

    kan_load_user(USER_SO);
    mdefer { kan_unload_user(); }
    kan_user_funcs.user_set_state(&state);
    kan_user_funcs.user_init();
    mdefer { kan_user_funcs.user_deinit(); }

    SetTargetFPS(60);

    while(!WindowShouldClose()) {
        bool reload_requested = kan_user_funcs.user_new_frame();

        if (reload_requested) {
            mibs_log(MIBS_LL_INFO, "User library has requested a reload\n");
            kan_load_user(USER_SO);
            kan_user_funcs.user_set_state(&state);
            kan_user_funcs.user_after_reload();
        }
    }


    return 0;
}

