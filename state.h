#ifndef STATE_H_
#define STATE_H_

typedef struct {
    Color bg_color;
    Kan_Tasks tasks;
    Kan_Da(void*) alloced_tasks;
    bool running; // paused or running
} State;

#endif // STATE_H_
