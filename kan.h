#ifndef KAN_H_
#define KAN_H_

#include <dlfcn.h>
#include <string.h>
#include <stdlib.h>

#include "raylib.h"

// -------------------------------------------------------
// Utils
// -------------------------------------------------------

#ifndef KAN_MALLOC
#   define KAN_MALLOC malloc
#endif

#ifndef KAN_FREE
#   define KAN_FREE free
#endif

#ifndef KAN_REALLOC
#   define KAN_REALLOC realloc
#endif

#define KAN_MACRO_UNPACK(...) __VA_ARGS__

#define Kan_Da(T) struct { T* items; size_t count; size_t cap; }

#define kan_da_deinit(da) \
    do { \
    	(da)->count = 0; \
        KAN_FREE((da)->items); \
    } while(0)

#define kan_da_append(da, item) \
    do { \
        if ((da)->count == 0) { \
            (da)->items = KAN_MALLOC(sizeof((item))); \
            (da)->cap = 1; \
        } else { \
        	if ((da)->count == (da)->cap) { \
	        	(da)->cap *= 2; \
		        (da)->items = KAN_REALLOC((da)->items, (da)->cap * sizeof((item))); \
        	} \
        } \
    	(da)->items[(da)->count++] = item; \
    } while(0)

#define kan_da_remove(da, index) \
    do { \
        for (size_t j = index; j < (da)->count; j++) { \
            (da)->items[j] = (da)->items[j+1]; \
        } \
        (da)->count -= 1; \
    } while(0)

/* ((void*)(ptr) - (void*)(da)->items) / sizeof(*(da)->items) */

/* #define kan_da_remove_ptr(da, ptr) \ */
/*     kan_da_remove((da), ((void*)(ptr) - (void*)(da)->items) / sizeof(*(da)->items)) */

// -------------------------------------------------------
// User
// -------------------------------------------------------

typedef struct {
    // initializes the user library
    bool (*user_init)(void);
    // deinitializes the user library 
    bool (*user_deinit)(void); 
    // returns bool on reload request
    bool (*user_new_frame)(void); 
    // sets the state of the user library
    void (*user_set_state)(void*);
    // fixes function pointers after plugin reload
    void (*user_after_reload)(void); 
} Kan_User_Funcs;

extern Kan_User_Funcs kan_user_funcs;
extern void* kan_user;

void kan_load_user(const char* so);
void kan_unload_user(void);

// -------------------------------------------------------
// Tasks
// -------------------------------------------------------

typedef struct {
    int id;
    void (*update)(void* data);
    void* self_ptr;
    bool complete;
} Kan_Task_Base;

#define Kan_Task(StateT) struct { \
    KAN_MACRO_UNPACK StateT state; \
    Kan_Task_Base base; \
}

typedef Kan_Da(Kan_Task_Base) Kan_Tasks;

#define kan_new_task(TaskT, _state, _id, _update) \
({ \
    TaskT* _task = (TaskT*)KAN_MALLOC(sizeof(TaskT)); \
    _task->base = (Kan_Task_Base){ \
        .id = (_id), \
        .update = (_update), \
        .self_ptr = _task, \
        .complete = false, \
    }; \
    memset(&_task->state, 0, sizeof(_task->state)); \
    kan_da_append(&(_state)->tasks, _task->base); \
    kan_da_append(&(_state)->alloced_tasks, _task); \
    _task; \
})

#endif // KAN_H_
