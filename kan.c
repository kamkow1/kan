#include <stdio.h>
#include "kan.h"

// -------------------------------------------------------
// User
// -------------------------------------------------------

Kan_User_Funcs kan_user_funcs = {0};
void* kan_user = 0;

void kan_load_user(const char* so)
{
    if (kan_user) dlclose(kan_user);

    kan_user = dlopen(so, RTLD_NOW);
    if (!kan_user) {
        printf("Error: Failed to open %s\n", so);
    }

    kan_user_funcs.user_init = dlsym(kan_user, "user_init");
    kan_user_funcs.user_deinit = dlsym(kan_user, "user_deinit");
    kan_user_funcs.user_after_reload = dlsym(kan_user, "user_after_reload");
    kan_user_funcs.user_new_frame = dlsym(kan_user, "user_new_frame");
    kan_user_funcs.user_set_state = dlsym(kan_user, "user_set_state");
}

void kan_unload_user(void)
{
    if (kan_user) dlclose(kan_user);
}

