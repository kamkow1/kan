#define MIBS_IMPL
#define MIBS_USE_WRAPPERS
#include "mibs.h"

typedef bool (*Command_Func)(void);

typedef struct {
    const char* name;
    Command_Func func;
} Command;

typedef map_t(Command_Func) Commands;

bool command_help(void);
bool command_build(void);
bool command_clean(void);

static const Command commands[] = {
    { .name = "help",  .func = &command_help   },
    { .name = "build",  .func = &command_build },
    { .name = "clean",  .func = &command_clean },
};

bool command_help(void)
{
    mibs_log(MIBS_LL_INFO, "Usage: ./mibs <command> <arguments>\n");
    mibs_log(MIBS_LL_INFO, "Available commands:\n");
    for (size_t i = 0; i < mibs_array_len(commands); i++) {
        printf("%d: %s\n", i+1,commands[i].name);
    }
    return 1;
}

#define EXAMPLE_BUILD_DIR "build/"

#define EXAMPLE_DRIVER_OUT "build/kan_example"
#define EXAMPLE_DRIVER_SRC "example_driver.c", "kan.c"
#define EXAMPLE_DRIVER_CFLAGS \
    "-ggdb", \
    "-I./libs/raylib-5.0_linux_amd64/include"

#define EXAMPLE_DRIVER_LDFLAGS \
    "-Wl,-rpath=./build/", \
    "-Wl,-rpath=./", \
    "-Wl,-rpath=./libs/raylib-5.0_linux_amd64/lib/", \
    "-L./libs/raylib-5.0_linux_amd64/lib", \
    "-l:libraylib.so", \
    "-ldl"

#define EXAMPLE_USER_SRC "example_user.c", "kan.c"
#define EXAMPLE_USER_OUT "build/libexample_user.so"
#define EXAMPLE_USER_CFLAGS \
    "-ggdb", \
    "-I./libs/raylib-5.0_linux_amd64/include", \
    "-fPIC", \
    "-rdynamic", \
    "-shared"

#define EXAMPLE_USER_LDFLAGS \
    "-L./libs/raylib-5.0_linux_amd64/lib", \
    "-l:libraylib.so", \
    "-lm", \
    "-ldl", \
    "-lpthread", \
    "-lglfw"

bool command_build(void)
{
    mibs_log(MIBS_LL_INFO, "Building example %s %s\n", EXAMPLE_DRIVER_OUT, EXAMPLE_USER_OUT);

    if (!mPATH_EXISTS(EXAMPLE_BUILD_DIR)) {
        mibs_log(MIBS_LL_INFO, "Build directory doesn't exist. Creating directory %s\n", EXAMPLE_BUILD_DIR);
        mMKDIR(EXAMPLE_BUILD_DIR);
    }

    if (!mPATH_EXISTS("build/libraylib.so")) {
        mibs_log(MIBS_LL_INFO, "libraylib.so is missing from build directory. Copying libraylib.so\n");

        if (!mibs_copy_file("./libs/raylib-5.0_linux_amd64/lib/libraylib.so", "./build/libraylib.so", MIBS_FK_BIN)) {
            mibs_log(MIBS_LL_ERROR, "Failed to copy libraylib.so into build directory\n");
            return 0;
        }
    }

    bool ok = mCMD(MIBS_CC, "-o", EXAMPLE_DRIVER_OUT, EXAMPLE_DRIVER_CFLAGS, EXAMPLE_DRIVER_SRC, EXAMPLE_DRIVER_LDFLAGS);
    ok = ok && mCMD(MIBS_CC, "-o", EXAMPLE_USER_OUT, EXAMPLE_USER_CFLAGS, EXAMPLE_USER_CFLAGS, EXAMPLE_USER_SRC, EXAMPLE_USER_LDFLAGS);
}

bool command_clean(void)
{
    if (!mPATH_EXISTS(EXAMPLE_BUILD_DIR)) {
        mibs_log(MIBS_LL_INFO, "Directory %s doesn't exist yet\n", EXAMPLE_BUILD_DIR);
        return 0;
    }

    mibs_log(MIBS_LL_INFO, "Deleting directory %s\n", EXAMPLE_BUILD_DIR);

    return mRMDIR(EXAMPLE_BUILD_DIR);
}

#define CLI_ASSERT(cond) if (!(cond)) { command_help(); return 1; }

int main(int argc, char** argv)
{
    mREBUILD(argc, argv);

    // Init commands
    Commands command_map = {0};
    map_init(&command_map);
    mdefer { map_deinit(&command_map); }
    for (size_t i = 0; i < mibs_array_len(commands); i++) {
        map_set(&command_map, commands[i].name, commands[i].func);
    }

    CLI_ASSERT(argc >= 2);
    const char* command_name = *(++argv);

    Command_Func* command = map_get(&command_map, command_name);
    CLI_ASSERT(command != NULL);

    return !(*command)();
}

